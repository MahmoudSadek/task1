package com.sadek.creativemindstask1.model;

public class EventItem {

    String Title,time, gender, type, users;
    int image;
    boolean eventCanceled;

    public EventItem(String title, String time, String gender, String type, String users, int image, boolean eventCanceled) {
        Title = title;
        this.time = time;
        this.gender = gender;
        this.type = type;
        this.users = users;
        this.image = image;
        this.eventCanceled = eventCanceled;
    }

    public boolean getEventCanceled() {
        return eventCanceled;
    }

    public void setEventCanceled(boolean eventCanceled) {
        this.eventCanceled = eventCanceled;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }
}