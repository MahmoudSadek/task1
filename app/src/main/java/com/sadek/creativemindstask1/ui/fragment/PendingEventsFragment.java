package com.sadek.creativemindstask1.ui.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.sadek.creativemindstask1.R;
import com.sadek.creativemindstask1.adapter.AllEventsAdapter;
import com.sadek.creativemindstask1.model.EventItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PendingEventsFragment extends Fragment implements AllEventsAdapter.ClickView {

    Unbinder unbinder;


    @BindView(R.id.all_events_recycler)
    RecyclerView all_events_recycler;


    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    AllEventsAdapter allEventsAdapter;
    List<EventItem> eventList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_all_events, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        unbinder = ButterKnife.bind(this, view);

        initUI();


    }

    private void initUI() {
        eventList = new ArrayList<EventItem>();

        //all_events_recycler
        final RecyclerView.LayoutManager mLayoutManager_my_orders = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        all_events_recycler.setLayoutManager(mLayoutManager_my_orders);
        allEventsAdapter = new AllEventsAdapter(getContext(), eventList, this);
        all_events_recycler.setAdapter(allEventsAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_bright);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                    getData();

            }
        });

        swipeRefreshLayout.setRefreshing(false);
        getData();


    }


    private void getData() {
        eventList.clear();
        eventList.add(new EventItem("ابي اروح الهايبر وماعندي سياره ممكن حد يوديني", "منذ ساعة","رجال","فزعة سيارة","2 فزعو", R.drawable.type_car, false));
        eventList.add(new EventItem("بنات ضروري عندي عزومة وابي حد يساعدني", "منذ ساعة","سيدات","فزعة اكل","3 فزعو", R.drawable.type_food, false));
        allEventsAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void clickItem(int position) {

    }
}
