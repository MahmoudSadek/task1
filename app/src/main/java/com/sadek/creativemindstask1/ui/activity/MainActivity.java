package com.sadek.creativemindstask1.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.sadek.creativemindstask1.R;
import com.sadek.creativemindstask1.adapter.ViewPagerAdapter;
import com.sadek.creativemindstask1.ui.fragment.CanceledEventsFragment;
import com.sadek.creativemindstask1.ui.fragment.PendingEventsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    //the icons of tablayout  icon white  don't selected
    private int[] tabTxt = {
            R.string.canceled,
            R.string.approved,
            R.string.waiting,

    };
    @BindView(R.id.events_status_tabs)
    TabLayout tabLayout;
    @BindView(R.id.events_pager)
    ViewPager viewPager;
    @BindView(R.id.faz_type)
    TextView faz_type;
    @BindView(R.id.others_type)
    TextView others_type;
    int selectedType = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initUI();
    }
    private void initUI() {

        // inti the viewPager and set up it
        setupViewPager(viewPager);
        //inti the tab layout and it's icons
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        viewPager.setCurrentItem(2);
    }
    ViewPagerAdapter adapter;

    private void setupViewPager(ViewPager viewPager) {

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CanceledEventsFragment(), getString(tabTxt[0]));
        adapter.addFragment(new PendingEventsFragment(), getString(tabTxt[1]));
        adapter.addFragment(new PendingEventsFragment(), getString(tabTxt[2]));
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);


    }

    /**
     * set up the tab icons to the tab layout and inti the custom view to it
     */
    private void setupTabIcons() {

        final View[] selectedImageResources;

        final View view0 = getLayoutInflater().inflate(R.layout.custom_tab, null);
        ((TextView) view0.findViewById(R.id.txt_tab)).setText(tabTxt[0]);
        ((TextView) view0.findViewById(R.id.txt_tab)).setTextColor(getResources().getColor(R.color.colorPrimary));
        (view0.findViewById(R.id.tab_back)).setBackgroundResource(R.color.colorPrimaryDark);
        tabLayout.getTabAt(0).setCustomView(view0);

        View view1 = getLayoutInflater().inflate(R.layout.custom_tab, null);
        ((TextView) view1.findViewById(R.id.txt_tab)).setText(tabTxt[1]);
        ((TextView) view1.findViewById(R.id.txt_tab)).setTextColor(getResources().getColor(R.color.gray));
            (view1.findViewById(R.id.tab_back)).setBackgroundResource(android.R.color.transparent);
        tabLayout.getTabAt(1).setCustomView(view1);

            View view2 = getLayoutInflater().inflate(R.layout.custom_tab, null);
            ((TextView) view2.findViewById(R.id.txt_tab)).setText(tabTxt[2]);
            ((TextView) view2.findViewById(R.id.txt_tab)).setTextColor(getResources().getColor(R.color.gray));
        (view1.findViewById(R.id.tab_back)).setBackgroundResource(android.R.color.transparent);
            tabLayout.getTabAt(2).setCustomView(view2);


            selectedImageResources = new View[]{view2, view1 ,view0};


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ((TextView) selectedImageResources[tab.getPosition()].findViewById(R.id.txt_tab))
                        .setText(tabTxt[tab.getPosition()]);

                ((TextView) selectedImageResources[tab.getPosition()].findViewById(R.id.txt_tab)).setTextColor(getResources().getColor(R.color.colorPrimary));

                        (selectedImageResources[tab.getPosition()].findViewById(R.id.tab_back)).setBackgroundResource(R.color.colorPrimaryDark);

                tab.setCustomView(selectedImageResources[tab.getPosition()]);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                ((TextView) selectedImageResources[tab.getPosition()].findViewById(R.id.txt_tab))
                        .setText(tabTxt[tab.getPosition()]);

                ((TextView) selectedImageResources[tab.getPosition()].findViewById(R.id.txt_tab)).setTextColor(getResources().getColor(R.color.gray));

                        (selectedImageResources[tab.getPosition()].findViewById(R.id.tab_back)).setBackgroundResource(android.R.color.transparent);

                tab.setCustomView(selectedImageResources[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    //faz_type
    @OnClick(R.id.faz_type)
    void faz_type(View view) {
        others_type.setTextColor(getResources().getColor(R.color.white));
        faz_type.setTextColor(getResources().getColor(R.color.blac));
        faz_type.setBackgroundResource(R.drawable.curved_white);
        others_type.setBackgroundResource(android.R.color.transparent);
    }
    //others_type
    @OnClick(R.id.others_type)
    void others_type(View view) {
        faz_type.setTextColor(getResources().getColor(R.color.white));
        others_type.setTextColor(getResources().getColor(R.color.blac));
        others_type.setBackgroundResource(R.drawable.curved_white);
        faz_type.setBackgroundResource(android.R.color.transparent);
    }
}