package com.sadek.creativemindstask1.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sadek.creativemindstask1.R;
import com.sadek.creativemindstask1.model.EventItem;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllEventsAdapter extends RecyclerView.Adapter<AllEventsAdapter.ViewHolder> {

    private List<EventItem> contents;
    private Context mContext;
    ClickView clickView;

    public AllEventsAdapter(Context mContext, List<EventItem> contents, ClickView clickView) {
        this.contents = contents;
        this.mContext = mContext;
        this.clickView = clickView;
    }


    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event, parent, false);
        return new AllEventsAdapter.ViewHolder(view, clickView);


    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        try {

            holder.title_txt.setText(contents.get(position).getTitle());
            holder.time_txt.setText(contents.get(position).getTime());
            holder.gender_txt.setText(contents.get(position).getGender());
            holder.type_txt.setText(contents.get(position).getType());
            holder.users_txt.setText(contents.get(position).getUsers());

            Picasso.with(mContext).load(contents.get(position).getImage()).error(R.drawable.type_car).into(holder.event_cover_img);
            if(contents.get(position).getEventCanceled())
                holder.cancel_btn.setVisibility(View.GONE);
            else
                holder.cancel_btn.setVisibility(View.VISIBLE);
        } catch (Exception e) {
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.event_cover_img)
        ImageView event_cover_img;
        @BindView(R.id.title_txt)
        TextView title_txt;
        @BindView(R.id.time_txt)
        TextView time_txt;
        @BindView(R.id.gender_txt)
        TextView gender_txt;
        @BindView(R.id.type_txt)
        TextView type_txt;
        @BindView(R.id.users_txt)
        TextView users_txt;
        @BindView(R.id.cancel_btn)
        View cancel_btn;

        private ClickView mOnClickItem;

        ViewHolder(View view, ClickView clickView) {
            super(view);
            ButterKnife.bind(this, view);

            this.mOnClickItem = clickView;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mOnClickItem.clickItem(position);
            /*switch (view.getId()){
                case R.id.delete_booth:
                    mOnClickItem.clickItem(position);
                    break;


            }*/
        }

    }

    public interface ClickView {
        void clickItem(int position);
    }
}